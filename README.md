# Serverless NGINX App Protect (SNAP)
This repository automates NGINX App Protect WAF deployment and operations on AWS server-less platform (ECS). Therefore you can deploy production grade WAF in minutes without spending time on infrastructure management and operations automation. Such WAF deployment is similar to SaaS model. It protects multiple applications regardless of their location however you only pay for AWS resources.
## Architecture
Serverless NGINX App Protect consists of control plane and data plane. A pipeline in this repository acts as a control plane and automates WAF deplyment and operations. Pipeline runs on every commit. It deploys or chages WAF configuration in accordance with commit contents. There are three stages in the pipeline. First stage uses terraform to create and manage dataplane resources in AWS cloud. Second builds NGINX App Protect contoller image and pushes it to AWS ECR registry. Third deploys WAF container instances on top of server-less cluster created in first stage. Therefore any change in the repository automatically reflects to a running deployment.  
\
Dataplane consists of VPC resources, AWS application load balancer and WAF container instances deployed in AWS ECS server-less cluster. Dataplane doesn't require any manual intervention and fully controlled by the pipeline. Diagram below represents data plane architecture. 
![alt text](img/architecture.png "Architecture")
## Getting Started
Serverless NGINX App Protect WAF deployment includes following major steps:
1. Get NGINX App Protect repo access certificate and key from NGINX Inc. ([link](https://www.nginx.com/free-trial-request))
2. Create and setup AWS account
3. Clone and set up repository
Once all steps are done then CI/CD pipeline automatically sets up a dataplane.
### Setting up AWS account
1. Create AWS account.
2. Create a AWS IAM user with programmatic access only. Pipeline uses this user to manage AWS resources.
2. Create and download user's programmatic access credentials (access key id and secret access key)
2. Assign "AdministratorAccess" policy to the user.
3. Create "ecsTaskExecutionRole" role. This role allows NGINX App Protect container instances to store logs in AWS Cloudwatch.
4. Attach "CloudWatchFullAccess" policy to "ecsTaskExecutionRole" role.
5. Create S3 bucket with default permissions. This S3 bucket stores current deployment state.
### Cloning and setting up repository
1. Clone template repository. Use Gitlab GUI or following commands:
```
$ git clone https://gitlab.com/464d41/serverless-nginx-app-protect.git .
$ cd serverless-nginx-app-protect
$ git remote rm origin
$ git remote add origin YOUR_NEW_GITLAB_REPO_URL
$ git push origin master
```
2. Add following CI/CD variables to cloned repository.
* AWS_ACCESS_KEY_ID (user from previuous step)
* AWS_SECRET_ACCESS_KEY (user from previuous step)
* AWS_DEFAULT_REGION (Region where server-less NGINX App Protect will be deployed.. e.g.: us-east-2)
* BUCKET (S3 bucket name from previous step. E.g.: serverless-nap-demo)
* NGINX_REPO_CRT (Convert your App Protect repo access cert to base64 form. e.g.: base64 -w0 nginx-repo.crt)
* NGINX_REPO_KEY (Convert your App Protect repo access key to base64 form. e.g.: base64 -w0 nginx-repo.key)
3. Trigger a pipeline by making minor changes in "terraform/main.tf" and "build/Dockerfile" comments (e.g.: introduce an extra space in comment).
As soon as CI/CD pipline ends successfully your serverless NGINX App Protect is ready to use. Open URL of newly created "snap-alb" AWS load balancer in browser to access your  deployment.
## Modifying configuration
Consider serverless NGINX App Protect as well-known NGINX running in a cloud with configuration files stored in Gitlab repo. Any change in the repository automatically reflect to a running deployment.
### NGINX Configuration
Commit changes to "deploy/nginx.conf" to modify NGINX configuration. Use official documentation for [reference](https://www.nginx.com/resources/wiki/start/topics/examples/full/).
### App Protect WAF Configuration
Commit changes to "deploy/NginxDefaultPolicy.json" to modify App Protect WAF policy. Use official documentation for [reference](https://docs.nginx.com/nginx-app-protect/configuration/).
## Roadmap
1. Serverless NGINX App Protect in AWS (Supported)
2. Serverless NGINX App Protect in GCP (In development)
3. Serverless NGINX App Protect in AWS (Future)
