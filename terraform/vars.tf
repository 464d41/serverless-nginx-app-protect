variable "region" {
}

variable "cidr_block" {
  default = "10.11.0.0/16"
}

variable "public_subnet_a_cidr" {
  default = "10.11.0.0/25"
}

variable "public_subnet_b_cidr" {
  default = "10.11.128.0/25"
}

variable "subnet_a_cidr" {
  default = "10.11.1.0/24"
}

variable "subnet_b_cidr" {
  default = "10.11.2.0/24"
}
